"""
Script python pour ouvrir les fichiers de traces de clavier

"""

import matplotlib.pyplot as plt
import numpy as np
import time
import os
import random

def read_int(f):
    ba = bytearray(4)
    f.readinto(ba)
    prm = np.frombuffer(ba, dtype=np.int32)
    return prm[0]
    
def read_double(f):
    ba = bytearray(8)
    f.readinto(ba)
    prm = np.frombuffer(ba, dtype=np.double)
    return prm[0]

def read_double_tab(f, n):
    ba = bytearray(8*n)
    nr = f.readinto(ba)
    if nr != len(ba):
        return []
    else:
        prm = np.frombuffer(ba, dtype=np.double)
        return prm
    
def get_pics_from_file(filename):
    # Lecture du fichier d'infos + pics detectes (post-processing KeyFinder)
    print("Ouverture du fichier de pics "+filename)
    f_pic = open(filename, "rb")
    info = dict()
    info["nb_pics"] = read_int(f_pic)
    print("Nb pics par trame: " + str(info["nb_pics"]))
    info["freq_sampling_khz"] = read_double(f_pic)
    print("Frequence d'echantillonnage: " + str(info["freq_sampling_khz"]) + " kHz")
    info["freq_trame_hz"] = read_double(f_pic)
    print("Frequence trame: " + str(info["freq_trame_hz"]) + " Hz")
    info["freq_pic_khz"] = read_double(f_pic)
    print("Frequence pic: " + str(info["freq_pic_khz"]) + " kHz")
    info["norm_fact"] = read_double(f_pic)
    print("Facteur de normalisation: " + str(info["norm_fact"]))
    tab_pics = []
    pics = read_double_tab(f_pic, info["nb_pics"])
    nb_trames = 1
    while len(pics) > 0:
        nb_trames = nb_trames+1
        tab_pics.append(pics)
        pics = read_double_tab(f_pic, info["nb_pics"])
    print("Nb trames: " + str(nb_trames))
    f_pic.close()
    return tab_pics, info

def get_pics_from_file_no_print(filename):
    # Lecture du fichier d'infos + pics detectes (post-processing KeyFinder)
    f_pic = open(filename, "rb")
    info = dict()
    info["nb_pics"] = read_int(f_pic)
    info["freq_sampling_khz"] = read_double(f_pic)
    info["freq_trame_hz"] = read_double(f_pic)
    info["freq_pic_khz"] = read_double(f_pic)
    info["norm_fact"] = read_double(f_pic)
    tab_pics = []
    pics = read_double_tab(f_pic, info["nb_pics"])
    nb_trames = 1
    while len(pics) > 0:
        nb_trames = nb_trames+1
        tab_pics.append(pics)
        pics = read_double_tab(f_pic, info["nb_pics"])
    f_pic.close()
    return tab_pics, info

def compute_means(pics): # return array of means
    means = [0] * len(pics[0])
    for i in range(0, len(pics)):
        pic = pics[i]
        for j in range(0, len(pic)):
            mean = means[j] * j
            mean = (mean + pic[j]) / (j + 1)
            means[j] = mean
    return means
    
    
def compute_pic(pic):
    res = []
    for i in range(0, len(pic) - 1):
        res += [pic[i]]
        res += [pic[i + 1] - pic[i]] # variation
    res += [pic[len(pic) - 1]]
    return res

def filter_pic(pics, means):
    # 0.3 of error, 6 non valid pics
    valid_pics = []
    for i in range(0, len(pics)):
        invalid_count_pic = 0
        pic = pics[i]
        for j in range(0, len(pic)):
            if (abs(means[j] - pic[j]) > 0.3): # invalide pic
                invalid_count_pic += 1
        if (invalid_count_pic <= 4):
            valid_pics += [pic]
    return valid_pics
 
def generate_data(pics):
    for i in range(5800):
        rand1 = np.random.randint(5800)
        rand2 = np.random.randint(5800)
        new_pic = []
        for j in range(0, len(pics[rand1])):
            new_pic += [(pics[rand1][j] + pics[rand2][j]) / 2]
        
        pics += [new_pic]
    return pics
    
def get_set_of_file(filename):
    pic, info = get_pics_from_file_no_print(filename)
    means = compute_means(pic)
    valid_pics = filter_pic(pic, means)
    new_pics = generate_data(valid_pics)
    for i in range(0, 4):
        new_pics = generate_data(new_pics)
    pic_with_variation = []
    for i in range(0, len(new_pics)):
        pic_with_variation += [compute_pic(new_pics[i])]
    random.shuffle(pic_with_variation)
    return pic_with_variation[:28800], pic_with_variation[28800:31800], pic_with_variation[31800:34800]

def get_loginMdp():
    pic, info = get_pics_from_file_no_print('../data/pics_LOGINMDP.bin')
    pic_with_variation = []
    for i in range(0, len(pic)):
        pic_with_variation += [compute_pic(pic[i])]
    return pic_with_variation

def get_all_sets(path):
    input_train_res = []
    target_train_res = []
    train_res = (input_train_res, target_train_res)
    input_test_res = []
    target_test_res = []
    test_res = (input_test_res, target_test_res)
    input_validation_res = []
    target_validation_res = []
    validation_res = (input_validation_res, target_validation_res)
    login = False
    keys = []
    for index, filename in enumerate(os.listdir(path)):
        if filename == "pics_LOGINMDP.bin":
            login = True
            continue
        
        key = filename[5:len(filename) - 4]
        keys += [key]
        train, test, validation = get_set_of_file(os.path.join(path, filename))
        input_train_res += train
        input_test_res += test
        input_validation_res += validation
        for i in range(len(train)):
            res = np.zeros(42)
            res[index - login] = 1
            target_train_res += [res]
            if i < len(test):
                target_test_res += [res]
            if i < len(validation):
                target_validation_res += [res]

    return train_res, test_res, validation_res, keys
